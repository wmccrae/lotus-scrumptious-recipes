from django.urls import path
from .views import signup, user_login, user_logout


urlpatterns = [
    path("signup/", signup, name="create_user"),
    path("login/", user_login, name="login_user"),
    path("logout/", user_logout, name="logout_user")
]
