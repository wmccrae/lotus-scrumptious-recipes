from django.db import models
from django.conf import settings

# Create your models here.
class Recipe(models.Model): # Inheriting from DJango class models.Model
    # These following things are the datatypes stored in a table in
    # the database.
    title = models.CharField(max_length=200) # A column of titles
    picture = models.URLField() # A column of pictures
    description = models.TextField() # A column of descriptions
    created_on = models.DateTimeField(auto_now_add=True)
    # Auto-save the date the recipe was first created.
    gluten_free = models.BooleanField(null=True)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title


# model for recipe steps
class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        'Recipe',
        related_name="steps",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["step_number"]


# model for recipe ingredients
class RecipeIngredients(models.Model):
    food_item = models.CharField(max_length=250)
    amount = models.CharField(max_length=250)
    recipe = models.ForeignKey(
        'Recipe',
        related_name="ingredients",
        on_delete=models.CASCADE,
    )
