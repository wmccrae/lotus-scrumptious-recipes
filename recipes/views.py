from django.shortcuts import render, get_object_or_404, redirect
from .models import Recipe
from .forms import RecipeForm
from django.contrib.auth.decorators import login_required

# Create your views here.
# This function is for showing the requested recipe.
def show_recipe(request, id):
    requested_recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": requested_recipe
    }
    # The render function already knows to look in the "templates" folder.
    # Without that folder the render will NOT work.
    return render(request, "recipes/detail.html", context)


# This function is for showing the complete list of recipes.
def recipe_list(request):
    recipes_list = Recipe.objects.all()
    context = {
        "recipe_list": recipes_list,
    }
    return render(request, "recipes/list.html", context)


# This function is for showing all of the current user's recipes.
# The 'login_required' decorator makes this view accessible only if
# the user is logged in.
@login_required
def my_recipe_list(request):
    recipes_list = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes_list,
    }
    return render(request, "recipes/list.html", context)


# This function is for creating a recipe page.
# The 'login_required' decorator makes this view accessible only if
# the user is logged in.
@login_required
def create_recipe(request):
    if request.method == "POST":
        # We should use the form to validate the values
        #   and save them to the database
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            # If all goes well, we can redirect the browser
            #   to another page and leave the function
            return redirect("show_recipe_list")
    else:
        # Create an instance of the Django model form class
        form = RecipeForm()
    # Put the form in the context
    context = {
        "form": form,
    }
    # Render the HTML template with the form
    return render(request, "recipes/create.html", context)


# This function shows a page that allows users to edit recipes.
@login_required
def edit_recipe(request, id):
    recipe_to_edit = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe_to_edit)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe_to_edit)

    context = {
        "recipe_object": recipe_to_edit,
        "recipe_form": form,
    }
    return render(request, "recipes/edit.html", context)
