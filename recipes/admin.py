from django.contrib import admin

from .models import Recipe, RecipeStep, RecipeIngredients

# Register your models here.
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = ("title", "description", "created_on", "gluten_free", "id")


@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = ("step_number", "instruction", "id")


@admin.register(RecipeIngredients)
class RecipeIngredientsAdmin(admin.ModelAdmin):
    list_display = (
        "food_item",
        "amount",
        "id"
    )
